import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  MensajePadre1 = 'Hola hijo';
  MensajePadre2 = 'Hijo ve a comprar pan';
  MensajePadre3 = 'Hijo ve con cuidado';
  MensajePadre4 = 'No te olvides llamar';
  MensajePadre5 = 'feliz cumpleaños';


  constructor() { }

  ngOnInit(): void {
  }

}
