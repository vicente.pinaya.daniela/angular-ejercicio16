import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Input() Mensajehijo1!: string;
  @Input() Mensajehijo2!: string;
  @Input() Mensajehijo3!: string;
  @Input() Mensajehijo4!: string;
  @Input() Mensajehijo5!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
